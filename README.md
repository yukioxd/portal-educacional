# Portal Educacional #

## Estágio atual: MVP ##

### Principais Recursos ###

1. #### O Administrador do portal poderá: ####
    1. Criação/Edição do Cadatro de Tipos de Usuários
    1. Criação/Edição do Cadastro de Usuários (Por ex.: `Professor` é um tipo de usuário, o `Aluno` e `Administrador` também é outro tipo de usuário )
    1. Envio de E-mails para os Usuários Específicos, ou então, para todos os usuários de um tipo específico.
    1. Criação, remoção das categorias das `matérias ou cursos`.
    1. Aprovação, Edição ou remoção dos conteúdos disponibilizado pelos professores.
    1. `Permitir` ou `Revogar` o acesso dos Alunos para acesso dos conteúdos, incluindo as `matérias ou cursos` .

2. #### Os proforessores com as permissões poderão disponibilizar os itens abaixo para os alunos: ####
    1. Aulas através de vídeos, com ou sem descrição.
    2. Provas ou lista de exercícios.
    3. Arquivos e materiais de auxílio.
    4. Certificado de conclusão do `matéria ou curso`.

3. #### Os alunos poderão: ####
    1. Realizar o cadastro, edição do próprio cadastro pelo portal.
    1. Explorar pelas `matérias ou cursos`, através de `categorias`, ou pesquisando com as `palavras chaves`.
    2. Realizar o pagamento para a conseguir acessar a `matérias ou curso` por período determinado ou indeterminado.
---
##### Campos para cadastro de tipo de usuários #####
1. Nome do tipo*
1. Permissões de acessos*
##### Campos para cadastro dos usuários #####
1. Nome completo*
1. E-mail*
1. CPF*
1. Data de nascimento*
1. CEP*
1. Cidade*
1. Estado*
1. Número de Telefone*
##### Campos para cadastro de tipo matérias #####
1. Nome do tipo*
1. Descrição do tipo*
##### Campos para cadastro matérias #####
1. Nome da `matéria ou curso`*
1. Descrição
1. Aula(arquivo em vídeo)
1. Arquivos de auxílio(arquivo)
1. Quantidade de dias de acesso após a permissão concedida.

* *campos obrigatórios